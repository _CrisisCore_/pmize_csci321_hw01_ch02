<?php
        /* Exercise 2-1, Step 5, part 1
        The _POST function retrieved the parameter entered by the user
        and assigns it to an array (to be commented out and replaced in 
        step 6, part 1) 
        */
        //$product_description = $_POST['product_description'];
        //$list_price = $_POST['list_price']; 
        //$discount_percent = $_POST['discount_percent'];
        
        /* Exercise 2-1, Step 6, part 1
        The filter input function can check if you use a certain input,
        that is valid according to a constant parameter you provide. INPUT_POST
        specifies the $_POST array. Note: We did not yet add a third parameter 
        to filter according to data type.  
        */
        $product_description = filter_input(INPUT_POST, 'product_description');
        $list_price = filter_input(INPUT_POST, 'list_price'); 
        $discount_percent = filter_input(INPUT_POST, 'discount_percent');
        /*Exercise 2-1, Step 7, part 1
         * Here I introduce a new variable that will be the calculation
         * of the discount amount
         */
        $discount_amount = $list_price * $discount_percent * .01;
        /*Exercise 2-1, Step 7, part 2
         * Here I introduce a new variable that will be the calculation
         * of the discount price
         */
        $discount_price = $list_price - $discount_amount;
        /* Exercise 2-1, Step 8, part 1
         * Adds formatted variable of list_price, note that the dollar sign is 
         * not formatting, but string concatenation, but the number_format 
         * function adds formatting such as a decimal and two zeros for currency
         */
        $list_price_f = "$".  number_format($list_price, 2); 
         /* Exercise 2-1, Step 8, part 2
         * Adds a new discount_percent variable with strig concatenation,
         * and note that no format function is necessary here
         */
         $discount_percent_f = $discount_percent."%";
         /* Exercise 2-1, Step 8, part 3
         * Adds formatted variable of discount_amount, note that the dollar sign is 
         * not formatting, but string concatenation, but the number_format 
         * function adds formatting such as a decimal and two zeros for currency
         */
         $discount_amount_f = "$".number_format($discount_amount, 2);
         /* Exercise 2-1, Step 8, part 4
         * Adds formatted variable of discount_price, note that the dollar sign is 
         * not formatting, but string concatenation, but the number_format 
         * function adds formatting such as a decimal and two zeros for currency
         */
         $discount_price_f = "$".number_format($discount_price, 2);
        
         /* Exercise 2-1 Enhancement Step 14:
         * Add the if else statement to check for invalid data, 
         * required fields, etc
         */
         // Makes sure the product description is not empty
        if ($product_description == NULL) {
            $error_message = 'Product description is a required field.';
        }
        // Validates list price
        else if ($list_price === FALSE) {
            $error_message = 'List Price must be a valid number.';
        }
        // Makes sure list price is greater than 0
        else if ($list_price <= 0) {
            $error_message = 'List price must be greater than 0.';
        } 
        // Validates discount percent
        else if ($discount_percent === FALSE) {
            $error_message = 'Discount percent must be a valid whole number.';
        }
        // Makes sure discount percent is greater than 0
        else if ($discount_percent <= 0) {
            $error_message = 'Discount percent must be greater than 0.';
        } 
        // set error message to an empty string if no invalid entries
        else {
            $error_message = '';
        }
        
        /* Exercise 2-1 Enhancement, Step 15: 
         * If an error message exists here, the index page is loaded
         * Note: The application will exit after this include statement
         */
        if ($error_message != '') {
            include('index.php');
            exit();
        }
    ?>
<!DOCTYPE html>
<html>
<head>
    <title>Product Discount Calculator</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <main>
        <!-- <h1>This page is under construction</h1> -->
        <!-- Exercise 2-1, Step 11: Change header to Product Discount Calculator -->
        <h1> Product Discount Calculator </h1>

         <!-- Exercise 2-1, Step 5, part 2: added the variable product_description that we
        created in Step 1 above to be displayed -->
        
        <label>Product Description:</label>
        <span><?php //echo $product_description 
        /*Exercise 2-1, Step 10, part 1
         * Added the html special char function
         */
        echo htmlspecialchars($product_description); ?></span><br>

        <!-- Exercise 2-1, Step 5, part 3: added the variable list_price that we
        created in Step 1 above to be displayed (To be replaced by formatted variable
        in step 8, part 1) -->
        <label>List Price:</label>
        <span><?php //echo $list_price;
        /* Execise 2-1, Step 8, part 5
         * Here we replace the original list_price variable to echo the new formatted
         * list price
         */
        //echo $list_price_f
        
        /* Exercise 2-1, Step 10, part 2
         * Added the html special char function
         */
         echo htmlspecialchars($list_price_f); ?></span><br>

        <!-- Exercise 2-1, Step 5, part 4: added the variable discount_percent that we
        created in Step 1 above to be displayed (To be replaced by formatted variable
        in step 8, part 2)-->
        <label>Standard Discount:</label>
        <span><?php //echo $discount_percent; 
        /* Execise 2-1, Step 8, part 6
         * Here we replace the original discount_percent variable to echo the new formatted
         * discount_percent
         */
        //echo $discount_percent_f 
        
        /* Exercise 2-1, Step 10, part 3
         * Added the html special char function
         */
        echo htmlspecialchars($discount_percent_f); ?></span><br>

        <!-- Exercise 2-1, Step 7, part 3: added the variable discount_amount that we
        created in Step 7, part 1 above to be displayed 
        (To be replaced by formatted variable in step 8, part 3)-->
        <label>Discount Amount:</label>
        <span><?php //echo $discount_amount; 
        /* Execise 2-1, Step 8, part 7
         * Here we replace the original discount_amount variable to echo the new formatted
         * discount_amout_f
         */
        echo $discount_amount_f
        ?></span><br>
        <!-- Exercise 2-1, Step 7, part 4: added the variable discount_price that we
        created in Step 7, part 1 above to be displayed.
        (To be replaced by formatted variable in step 8, part 4)-->
        <label>Discount Price:</label>
        <span><?php //echo $discount_price;
        /* Execise 2-1, Step 8, part 8
         * Here we replace the original discount_price variable to echo the new formatted
         * discount_price_f
         */
        echo $discount_price_f ?></span><br>
    </main>
</body>
</html>