<?php
/* Exercise 2-1 Enhancement Step 13, part 1
 * Adds if methods to check if the variable is NOT set,
 * if NOT set, the default is an empty string
 */
if (!isset($product_description)) {
    $product_description = '';
}
/* Exercise 2-1 Enhancement Step 13, part 2
 * Adds if methods to check if the variable is NOT set,
 * if NOT set, the default is an empty string
 */
if (!isset($list_price)) {
    $list_price = '';
}
/* Exercise 2-1 Enhancement Step 13, part 3
 * Adds if methods to check if the variable is NOT set,
 * if NOT set, the default is an empty string
 */
if (!isset($product_discount)) {
    $product_discount = ''; 
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Product Discount Calculator</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>

<body>
    <main>
        <h1>Product Discount Calculator</h1>
        <!-- Exercise 2-1 Enhancement Step 16:  
        -- Here the empty function will check if the error message's
        -- is NOT empty and if it is, the class is set to an error
        -- in the paragraph tag, and then we display the message 
        -- with special characters. Note: Must separate ending 
        -- of a php statement and opening of html tag -->
        <?php if (!empty($error_message)) { ?>
            <p class ="error"><?php echo htmlspecialchars($error_message); ?></p>
        <?php } ?>
        
        <form action="display_discount.php" method="post">

            <div id="data">
                <label>Product Description:</label>
                <input type="text" name="product_description"><br>

                <label>List Price:</label>
                <input type="text" name="list_price"><br>

                <label>Discount Percent:</label>
                <input type="text" name="discount_percent"><span>%</span><br>
            </div>

            <div id="buttons">
                <label>&nbsp;</label>
                <input type="submit" value="Calculate Discount"><br>
            </div>

        </form>
    </main>
</body>
</html>